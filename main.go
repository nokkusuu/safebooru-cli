package main

import (
    "fmt"
    "os"
    "strconv"
    "strings"
    "flag"
    "net/http"
    "io/ioutil"
    "path/filepath"

    "./lib/fetch"
    "./lib/safebooru"
)

// TODO: find better way to express version
var __version = "1.0.2"

// TODO: find out if this is orthodox 
func init() {
    flag.Usage = func() {
        fmt.Println("safebooru-cli v" + __version + ", by nox")
        fmt.Println("usage: safebooru [options]\n")
        // -- //
        fmt.Println("-id string\n\tspecifies id of image to download")
        fmt.Println("-tags string\n\tspecifies tags to search for")
        fmt.Println("-o string\n\tspecifies directory to save image to")
    }
}

func main() {
    var output, id, tags string
    flag.StringVar(&output, "o", "", "Specifies output directory")
    flag.StringVar(&id, "id", "", "Fetch image by specified ID")
    flag.StringVar(&tags, "tags", "", "Fetch image by specified tags")

    flag.Parse()

    post, err := getPost(id, tags)
    
    // a few different errors can come up here
    if err != nil {
        if err.Error() == "unexpected end of JSON input" {
            fmt.Println("error: could not find image with specified parameters")
            os.Exit(1)
        } else {
            fmt.Println("error: could not parse json response")
            os.Exit(1)
        }
    }

    fmt.Println("getting image information... (id: " + strconv.Itoa(post.ID) + ")")
    head, err := http.Head(post.ImageURL())

    if err != nil {
        fmt.Println("error: could not gather information on image; attempting to continue...")
    }

    if head.StatusCode != http.StatusOK {
        fmt.Println("got invalid status code; attempting to continue...")    
    }
    size, err := strconv.Atoi(head.Header.Get("Content-Length"))
    filesize := strconv.Itoa(size / 1024)

    fmt.Println("downloading image (" + filesize + "K)...")
    resp, err := fetch.Request("get", post.ImageURL(), nil)

    if output == "" {output = post.Image} else {

        fl, err := os.Stat(output)
        if err != nil {
            fmt.Println("directory error; continuing...")
        }

        if fl.Mode().IsDir() {
            if (strings.HasSuffix(output, "/") || strings.HasSuffix(output, "\\")) {
                output += post.Image
            } else {
                output += "/" + post.Image
            }
        } else {
            output += filepath.Ext(post.Image)
        }
    }
    fmt.Println("writing file " + output + "...")
    ioutil.WriteFile(output, resp, 7777)
}

// TODO: make this cleaner
func getPost(id string, tags string) (safebooru.SafeResp, error) {
    if id != "" {
        fmt.Println("getting post with id " + id + "...")
        post, err := safebooru.GetPostById(id)
        if err != nil {return safebooru.SafeResp{}, err}
        return post, nil
    } else if tags != "" {
        fmt.Println("getting post with tags " + tags + "...")
        post, err := safebooru.GetPostRandom(tags)
        if err != nil {return safebooru.SafeResp{}, err}
        return post, nil
    } else {
        fmt.Println("getting random post...")
        post, err := safebooru.GetPostRandom("")
        if err != nil {return safebooru.SafeResp{}, err}
        return post, nil
    }
}