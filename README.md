# safebooru-cli
###### safebooru cli written in go

## faq
### what does it do?
it downloads anime pictures; what else did you expect?

### why safebooru?
because nsfw is bad

### what's in `./lib/`?
nested packages i wrote from scratch while making this project to make my life easier

### why are there `cmd` files for building and testing on windows but nothing for linux?
i move between windows and linux a lot and i just happened to be writing this on windows. could've used subsystem, i know... but i didn't... sorry.