package fetch

import (
	"io/ioutil"
	"net/http"
	"strings"
)

// TODO: make FetchOptions the default third parameter and complete it
/*
type FetchOptions struct {
	headers map[string]string
	body []byte
}
*/

//Request makes a http request using provided parameters
func Request(Type string, url string, headers map[string]string) ([]byte, error) {
	// allow lowercase types
	Type = strings.ToUpper(Type)

	// create client
	client := &http.Client{}

	// create request
	req, err := http.NewRequest(Type, url, nil)
	if err != nil {return nil, err}

	// add all specified headers
	if headers != nil {
		for key := range headers {
			req.Header.Add(key, headers[key])
		}
	}

	// perform request
	resp, err := client.Do(req)
	if err != nil {return nil, err}

	// read body
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {return nil, err}

	// close body when function ends
	defer resp.Body.Close()

	// return body
	return body, nil
}