package safebooru

import (
	"encoding/json"
	"math/rand"
	"time"
	"errors"

    /* TODO:
    make these imports cleaner
    unsure if it'd be better to make these standalone packages or nested packages
    */
	"../inquire"
	"../fetch"
)

//SafeResp a safebooru response
type SafeResp struct {
	Directory       string
	Hash            string
	Height          int
	ID              int         `json:"id"`
	Image           string
	Change          int64
	Owner           string
	ParentID        int         `json:"parent_id"`
	Rating          string
	Sample          bool 
	SampleHeight    int         `json:"sample_height"`
	SampleWidth     int         `json:"sample_width"`
	Score           int64
	Tags            string
	Width           int
}

//ImageURL method to get actual url from a safebooru response 
func (resp *SafeResp) ImageURL() string {
	return "https://safebooru.org/images/" + resp.Directory + "/" + resp.Image
}

//GetPosts gets first limit# posts for specified tag
func GetPosts(tags string, limit int) ([]SafeResp, error) {
	query := inquire.Query(map[string]string{
		"page": "dapi",
		"s": "post",
		"q": "index",
		"tags": tags,
		"limit": string(limit),
		"json": "1",
	})

	url := "https://safebooru.org/index.php" + query

	resp, err := fetch.Request("get", url, nil)
	if err != nil {return nil, err}

	var resps []SafeResp
	err = json.Unmarshal(resp, &resps)
	
	if err != nil {return nil, err}

	return resps, nil
}

//GetPostById gets a post using its id
func GetPostById(id string) (SafeResp, error) {
	query := inquire.Query(map[string]string{
		"page": "dapi",
		"s": "post",
		"q": "index",
		"id": id,
		"json": "1",
	})

	url := "https://safebooru.org/index.php" + query

	resp, err := fetch.Request("get", url, nil)
	if err != nil {return SafeResp{}, err}

	var sfr []SafeResp
	err = json.Unmarshal(resp, &sfr)

	if err != nil {return SafeResp{}, err}
	if len(sfr) < 1 {
		return SafeResp{}, errors.New("couldn't find post with specified id")
	}

	return sfr[0], nil
}

//GetPostRandom gets a random post using specified tags
func GetPostRandom(tags string) (SafeResp, error) {
	query := inquire.Query(map[string]string{
		"page": "dapi",
		"s": "post",
		"q": "index",
		"tags": tags,
		"limit": "100",
		"json": "1",
	})

	url := "https://safebooru.org/index.php" + query

	resp, err := fetch.Request("get", url, nil)
	if err != nil {return SafeResp{}, err}

	var resps []SafeResp
	err = json.Unmarshal(resp, &resps)

	if err != nil {return SafeResp{}, err}
	if len(resps) < 1 {
		return SafeResp{}, errors.New("couldn't find post with specified tags")
	}

	rand.Seed(time.Now().Unix())
	return resps[rand.Intn(len(resps))], nil
}