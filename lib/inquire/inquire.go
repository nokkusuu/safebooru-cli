package inquire

import "net/url"

//Query turns a string map into a query string
func Query(params map[string]string) string {
	var first = 1 // NOTE: is this unorthodox?
	var qstr = ""
	for k, v := range params {
		if first == 1 {
			qstr += "?"+url.QueryEscape(k)+"="+url.QueryEscape(v)
			first = 0
		} else {
			qstr += "&"+url.QueryEscape(k)+"="+url.QueryEscape(v)
		}
	}
	return qstr
}